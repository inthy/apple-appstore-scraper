"""
File that contains all of the project's files.
Authors: Alexey Shtern & Yonah Taurog
"""

import json
import re
import config_globals


RATINGS_COUNT_CONVERSION_DICT = {'K': 1000, 'M': 1000000}
SIZE_CONVERSION_DICT = {'KB': 0.001, 'MB': 1, 'GB': 1024}


class Primitive(object):
    """
    This is a parent class for all the data types scraped in the App store

    Attributes:
       name (str): The name of the app or a page of the App store
       url (str): The link to the app or page
    """

    def __init__(self, name, url):
        self.name = name
        self.url = url
        self.db_id = None

    def __repr__(self):
        return '\n'.join(str(key) + ': ' + str(value) for key, value in self.__dict__.items())

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)

    def set_db_id(self, db_id):
        self.db_id = db_id

    def get_sql_dict(self):
        return {'name': self.name, 'url': self.url}


class Genre(Primitive):
    # These classes are created intentionally, they may have additional fields in the future
    pass


class Letter(Primitive):
    # These classes are created intentionally, they may have additional fields in the future
    pass


class Price(object):
    def __init__(self, raw_price):
        self.raw_price = raw_price

    def __repr__(self):
        return self.raw_price

    def get_price_parsed(self):
        """Converts price from raw to float, takes into account config's region"""
        region_dict = config_globals.regions[config_globals.region_name]
        free_label = region_dict[config_globals.FREE_LABEL]
        mask = region_dict[config_globals.CURRENCY_MASK]
        if self.raw_price == free_label:
            return 0
        else:
            price_match = re.findall(mask, self.raw_price)
            try:
                if price_match:
                    if len(price_match) > 0:
                        return float(price_match[0])
            except ValueError:
                print('Could not parse formatted price')
                return 0
        return 0


class InAppPurchase(object):
    def __init__(self, name, raw_price):
        self.name = name
        self.price = Price(raw_price)

    def __repr__(self):
        return '\n'.join(str(key) + ': ' + str(value) for key, value in self.__dict__.items())


class PlayStoreInfo(object):
    """
    Class for enrichment data from Google® Play Store
    """
    def __init__(self, name, price, rating=None, ratings_count=None):
        self.ps_name = name
        self.ps_price = price
        self.ps_rating = rating
        self.ps_ratings_count = ratings_count

    def set_rating(self, rating):
        self.ps_rating = rating

    def set_ratings_count(self, ratings_count):
        self.ps_ratings_count = ratings_count

    def __repr__(self):
        return '\n'.join(str(key) + ': ' + str(value) for key, value in self.__dict__.items())


class App(Primitive):
    """
    This is a class for all the apps in the app store
    with all the descriptions of the app.
    """
    info_dict = None
    rating = 0
    ratings_count = 0
    description = None
    in_app_purchases = None
    play_store_info = None

    def __init__(self, name, url):
        super().__init__(name, url)

        self.appstore_id = None
        appstore_id_match = re.findall('id(\d+)', url)
        try:
            if appstore_id_match:
                if len(appstore_id_match) > 0:
                    self.appstore_id = int(appstore_id_match[0])
        except ValueError:
            print('Could not parse AppStore ID')

    def add_raw_info_dict(self, info_dict):
        self.info_dict = {}
        for key in config_globals.info_fields:
            if key in info_dict:
                self.info_dict[key] = info_dict[key]
        if config_globals.should_parse_in_app_purchases:
            self.parse_in_app_purchases(info_dict)

        if 'size' in self.info_dict:
            try:
                parsed_size = self.parse_size()
                self.info_dict['size'] = parsed_size
            except ValueError:
                self.info_dict['size'] = 0
                print('Could not parse size')

        if 'age rating' in self.info_dict:
            try:
                parsed_age_rating = self.parse_age_rating()
                self.info_dict['age rating'] = parsed_age_rating
            except ValueError:
                self.info_dict['age rating'] = 0
                print('Could not parse age rating')

    def parse_in_app_purchases(self, info_dict):
        if config_globals.key_in_app_purchases in info_dict:
            self.in_app_purchases = []
            raw_in_app_purchases_list = info_dict[config_globals.key_in_app_purchases].split('\n\n\n')
            for raw_in_app_purchase in raw_in_app_purchases_list:
                splitted = raw_in_app_purchase.split('\n')
                if len(splitted) == 2:
                    self.in_app_purchases.append(InAppPurchase(splitted[0], splitted[1]))

    def add_rating_info(self, rating, ratings_count):
        try:
            big_ratings_match = re.search('(\d+\.?\d*(K|M))', ratings_count)
            if big_ratings_match:
                raw_big_ratings = big_ratings_match.group()
                letter = raw_big_ratings[-1:]
                small_rating = float(raw_big_ratings[:-1])
                self.ratings_count = int(small_rating * RATINGS_COUNT_CONVERSION_DICT[letter])
            else:
                self.ratings_count = int(ratings_count.split(' ')[0])
            self.rating = float(rating)
        except ValueError:
            print('Error in parsing rating/rating count value')
        except IndexError:
            print('Error in parsing rating count')

    def add_description_info(self, description):
        self.description = description

    def parse_size(self):
        raw_size = self.info_dict['size']
        regex = '(\d+\.?\d*) (KB|MB|GB)'
        matches = re.findall(regex, raw_size)
        if len(matches) > 0 and len(matches[0]) > 1:
            match = matches[0]
            return float(match[0]*SIZE_CONVERSION_DICT[match[1]])
        else:
            print('Could not parse size')
            return 0

    def parse_age_rating(self):
        raw_age_rating = self.info_dict['age rating']
        regex = '(\d+)'
        matches = re.findall(regex, raw_age_rating)
        if len(matches) > 0:
            return int(matches[0])
        else:
            print('Could not parse age rating')
            return 0

    def get_formatted_price(self):
        if 'price' in self.info_dict:
            return Price(self.info_dict['price']).get_price_parsed()
        return 0

    def get_sql_dict(self):
        """Export params for SQL functions"""
        sql_dict = super().get_sql_dict()
        sql_dict['appstore_id'] = self.appstore_id
        sql_dict['seller'] = self.info_dict.get('seller', '')
        if len(sql_dict['seller']) == 0:
            sql_dict['seller'] = self.info_dict.get('provider', '')
        sql_dict['size'] = self.info_dict['size']
        sql_dict['age_rating'] = self.info_dict['age rating']
        sql_dict['category'] = self.info_dict['category']
        sql_dict['compatibility'] = self.info_dict['compatibility']
        sql_dict['copyright'] = self.info_dict['copyright']
        sql_dict['languages'] = self.info_dict['languages']
        return sql_dict

    def set_play_store_info(self, play_store_info: PlayStoreInfo):
        self.play_store_info = play_store_info

