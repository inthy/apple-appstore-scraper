"""
Config file handler.
Authors: Alexey Shtern & Yonah Taurog
"""

import config_globals
import configparser
import json
from pathlib import Path


def handle_config(config_file_path):
    """Reads config file if it exists or otherwise creates a new one from code and then reads it"""

    my_file = Path(config_file_path)
    if not my_file.is_file():
        create_config(config_file_path)
    try:
        read_config(config_file_path)
        return True
    except ValueError as value_error:
        print(value_error)
        return False
    except KeyError as key_error:
        print('Missing key: {}'.format(key_error))
        return False


def read_config(config_file_path):
    """Parsing config file"""

    config = configparser.ConfigParser()
    config.read(config_file_path)
    config_globals.url = config['URL_CONFIG']['BaseURL']
    config_globals.start_point = config['URL_CONFIG']['StartPoint']
    config_globals.max_retry = int(config['URL_CONFIG']['MaxRetry'])

    if 'LIMITS' in config:
        config_globals.max_genres = int(config['LIMITS'].get('Genres', 0))
        config_globals.max_items_per_genre = int(config['LIMITS'].get('ItemsPerGenre', 0))

    config_globals.should_parse_rating = bool(config['FIELDS']['ParseRating'])
    config_globals.should_parse_in_app_purchases = bool(config['FIELDS']['ParseInAppPurchases'])
    config_globals.should_parse_description = bool(config['FIELDS']['ParseDescription'])
    config_globals.info_fields = json.loads(config['FIELDS']['InfoFields'])
    config_globals.regions = json.loads(config['REGIONS']['regions'])

    config_globals.menu_class = config['PARSING_CONSTANTS']['MenuClass']
    config_globals.alphabet_list = config['PARSING_CONSTANTS']['AlphabetList']
    config_globals.next_button_class = config['PARSING_CONSTANTS']['NextButtonClass']
    config_globals.apps_grid = config['PARSING_CONSTANTS']['AppsGrid']
    config_globals.key_information_list = config['PARSING_CONSTANTS']['InformationList']
    config_globals.key_description = config['PARSING_CONSTANTS']['Description']
    config_globals.key_rating = config['PARSING_CONSTANTS']['Rating']
    config_globals.key_ratings_count = config['PARSING_CONSTANTS']['RatingsCount']
    config_globals.key_in_app_purchases = config['PARSING_CONSTANTS']['InAppPurchases']

    config_globals.gcs_should_enrich = bool(config['GOOGLE_API']['ShouldEnrichData'])
    config_globals.gcs_api_keys = json.loads(config['GOOGLE_API']['API_KEY'])
    config_globals.gcs_engine_id = config['GOOGLE_API']['SEARCH_ENGINE_ID']
    config_globals.gcs_url = config['GOOGLE_API']['REQUEST_URL']

    config_globals.db_type = config['DB']['Type']
    config_globals.db_commit_frequency = int(config['DB'].get('commit_frequency', 0))

    if config_globals.db_type == 'MySQL':
        config_globals.mysql_user = config['MySQL']['user']
        config_globals.mysql_password = config['MySQL']['password']
        config_globals.mysql_db_name = config['MySQL']['db_name']
        config_globals.mysql_host = config['MySQL']['host']

        if 'port' in config['MySQL']:
            config_globals.mysql_port = int(config['MySQL']['port'])
        if 'socket' in config['MySQL']:
            config_globals.mysql_socket = config['MySQL']['socket']
    elif config_globals.db_type == 'SQLite':
        config_globals.sqlite_db_file_path = config['SQLite']['file_path']
    else:
        raise ValueError('Wrong DB type')


def create_config(config_file_path):
    """Creating test base config"""
    config = configparser.ConfigParser()
    config['URL_CONFIG'] = {'BaseURL': 'https://itunes.apple.com/'}
    config['URL_CONFIG']['StartPoint'] = '/genre/ios-books/id6018'
    config['URL_CONFIG']['MaxRetry'] = str(5)
    config['FIELDS'] = {}
    config['FIELDS']['InfoFields'] = json.dumps(['price', 'seller', 'size', 'age rating',
                                                'category', 'compatibility', 'copyright', 'languages'])
    config['FIELDS']['ParseRating'] = str(True)
    config['FIELDS']['ParseInAppPurchases'] = str(True)
    config['FIELDS']['ParseDescription'] = str(True)

    config['LIMITS'] = {}
    config['LIMITS']['Genres'] = str(1)
    config['LIMITS']['ItemsPerGenre'] = str(30)

    config['REGIONS'] = {}
    config['REGIONS']['regions'] = json.dumps({"us": {config_globals.CURRENCY_NAME: "USD",
                                                      config_globals.CURRENCY_MASK: "\$(\d+\.?\d*)",
                                                      config_globals.FREE_LABEL: "Free"},
                                               "gb": {config_globals.CURRENCY_NAME: "GBP",
                                                      config_globals.CURRENCY_MASK: "£(\d+\.?\d*)",
                                                      config_globals.FREE_LABEL: "Free"}})

    config['PARSING_CONSTANTS'] = {}
    config['PARSING_CONSTANTS']['MenuClass'] = 'list column first'
    config['PARSING_CONSTANTS']['AlphabetList'] = 'list alpha'
    config['PARSING_CONSTANTS']['NextButtonClass'] = 'paginate-more'
    config['PARSING_CONSTANTS']['AppsGrid'] = 'selectedcontent'
    config['PARSING_CONSTANTS']['InformationList'] = 'information-list'
    config['PARSING_CONSTANTS']['Rating'] = 'we-customer-ratings__averages__display'
    config['PARSING_CONSTANTS']['RatingsCount'] = 'we-customer-ratings__count'
    config['PARSING_CONSTANTS']['Description'] = 'section__description'
    config['PARSING_CONSTANTS']['InAppPurchases'] = 'in-app purchases'

    config['DB'] = {}
    config['DB']['Type'] = 'MySQL'

    config['MySQL'] = {}
    config['MySQL']['user'] = 'root'
    config['MySQL']['password'] = 'test'
    config['MySQL']['db_name'] = 'appstore_scraper'
    config['MySQL']['host'] = 'localhost'
    config['MySQL']['port'] = str(33060)
    config['MySQL']['socket'] = '/tmp/mysql.sock'

    config['GOOGLE_API'] = {}
    config['GOOGLE_API']['ShouldEnrichData'] = str(True)
    config['GOOGLE_API']['REQUEST_URL'] = 'https://www.googleapis.com/customsearch/v1'
    config['GOOGLE_API']['API_KEY'] = json.dumps(['AIzaSyDNZcry5oBy0INeBSSw32NL1hGAeplemfc'])
    config['GOOGLE_API']['SEARCH_ENGINE_ID'] = '017527637837067833136:xquaq9rfikw'

    with open(config_file_path, 'w') as configfile:
        config.write(configfile)
