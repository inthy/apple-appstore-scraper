"""
Database helper common interface.
Authors: Alexey Shtern & Yonah Taurog
"""


class DBHandler(object):
    """
    SQLite3 Database helper file.
    Authors: Alexey Shtern & Yonah Taurog
    """

    __connection__ = None
    __cursor__ = None

    def get_cursor(self):
        """Cursor singleton function"""
        pass

    def close_connection(self):
        """Close connection singleton function"""
        pass

    def commit(self):
        """Commit singleton function"""
        pass

    def configure_db(self):
        """Creates if needed and configures the DB"""
        pass

    def setup_region(self):
        """Loads regions from config to DB and performs necessary setup"""
        pass

    def delete_db(self):
        """Function for deleting DB file"""
        pass

    def create_db(self):
        """Function that creates DB and all of the tables"""
        pass

    def insert_get_key(self, values_dict: dict, table, unique='name'):
        """Function for inserting or updating entries and getting DB ID"""
        pass

    def save_popular(self, region_id, genre, genre_apps_list):
        """Saving popular rankings into DB"""
        pass

    def save_in_app_purchases(self, region_id, app):
        """Saving In-App Purchases in DB. Doesn't have commit() intentionally"""
        pass

    def save_regional_info(self, region_id, app):
        """Saving regional info in DB. Doesn't have commit() intentionally"""
        pass

    def save_playstore_data(self, app):
        """Saving PlayStore data for specified app"""

    def test_insert(self):
        """Dummy insert function"""
        pass

    def get_all_contents(self):
        """Print all tables' contents"""
        pass




