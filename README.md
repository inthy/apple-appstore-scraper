# AppStore Scraper

## About
This project is designed for scraping applications data from AppStore web catalogue. It is developed in educational purposes for ITC Fellows Spring Program 2019.

AppStore's apps catalogue page is split up to Genres. Each Genre page starts by default from Most Popular list and also has an alphabet index of all of the apps.

## Getting Started

The project's main and only script (for now) is called *appstore_scraper.py*. It requires *base_config.ini* configuration file for running.

## Configuring scraper

Current version of scraper builds default config file itself if function `create_config` is called.

`URL Config`

|Parameter   | Meaning  |
|---|---|
|  *edition* | AppStore's region.<br/> *Example: 'us' for USA*  |
|  *baseurl*  | AppStore's URL.<br/> *Example: https://itunes.apple.com/*  |
|  *startpoint* | First URL for starting scraper's navigation.<br/> *Example: /genre/ios-books/id6018 for books section*   |
|  *maxretry* | Number of maximum retries in case of fault from server  |
|  *mode* | Possible values:<br/> 0 - scraping all apps from each category.<br/> 1 - scraping only popular apps from each category  |

`FIELDS`

*This config section manages the amount of information we want to parse from every app's page.* <br/>
*Example of an app page:* [https://itunes.apple.com/il/app/telegram-messenger/id686449807?mt=8]

|Parameter   | Meaning  |
|---|---|
|  *infofields* | Array of comma-separated strings with names for parsing from app's info page.<br/>  *Example: \["price", "seller", "size"\]*  |
|  *parserating* | Bool flag, that indicates if scraper should try to parse rating from app's page  |
|  *parseinapppurchases* | Bool flag, that indicates if scraper should try to parse in app purchases from app's page  |
|  *parsedescription* | Bool flag, that indicates if scraper should try to parse description from app's page  |


`LIMITS`

|Parameter   | Meaning  |
|---|---|
|  *genres* | Number that defines the maximum number of genres to parse |
|  *itemspergenre* | Number that defines the maximum number of apps per genre to parse  |


`STDOUT`

|Parameter   | Meaning  |
|---|---|
|  *verbose* | Bool flag, if enabled prints parsed app description to console |


`PARSING_CONSTANTS`

List of service constants for parsing HTML pages



## Authors

* **Alexey Shtern** - [Inthy](https://bitbucket.org/inthy)
* **Yonah Taurog** - [xtreemtg](https://bitbucket.org/xtreemtg)


## License

No license yet
