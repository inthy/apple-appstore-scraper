"""
Apple Appstore Scraper.
Authors: Alexey Shtern & Yonah Taurog
"""

import requests
import random
import json
import config_handler
import config_globals
from classes import App, Genre, Letter, PlayStoreInfo
from bs4 import BeautifulSoup
from time import sleep
import argparse
from sqlite_handler import SQLiteHandler
from mysql_handler import MySQLHandler
from db_handler import DBHandler

HTML_PARSER = 'html.parser'
db_handler = DBHandler()


def build_scrape_link():
    """
    builds and returns the full link consisting of the URL to the app store,
    the country code, and the starting genre
    """
    return config_globals.url + config_globals.region_name + config_globals.start_point


def fetch_raw_data_requests(request_url, params=None):
    """
    takes in a url string and returns the content of the request data, given that the request
    is successful
    """
    for attempt in range(config_globals.max_retry):
        raw_data = requests.get(request_url, params)
        if raw_data.status_code == requests.codes.ok:
            return raw_data.content
        sleep(random.random() + 1)
    return None


def parse_primitive_entry(raw_genre, primitive_type):
    """
    given a soup tag of the raw data, return the correct instance of the data with
    its name and url
    """
    a = raw_genre.find('a')
    parsed_url = a.get('href', '')
    name = a.get_text()
    return primitive_type(name, parsed_url)


def name_for_class(primitive_type):
    """
    determines the type of the data and returns the necessary
    string that represents the type in the HTML.
    """
    class_field_dict = {Genre: config_globals.menu_class, Letter: config_globals.alphabet_list}
    if primitive_type in class_field_dict:
        return class_field_dict[primitive_type]
    return None


def parse_list(raw_data, primitive_type, limit_list=None):
    """
    parses the raw data from a page using BeautifulSoup.
    given the correct type for that data, the specific data is extracted
    for that type and is made as an instance of an object. this method then returns
    a list of those objects.
    """
    soup = BeautifulSoup(raw_data, HTML_PARSER)
    class_name = name_for_class(primitive_type)
    raw_list = soup.find(class_=class_name)
    parsed_list = []
    for raw_item in raw_list:
        parsed_list.append(parse_primitive_entry(raw_item, primitive_type))
    if not limit_list or len(limit_list) == 0:
        return parsed_list
    result_list = [parsed for parsed in parsed_list if parsed.name in limit_list]
    if len(result_list) > 0:
        return result_list
    else:
        return parsed_list


def find_next_button(raw_data):
    """
    this function takes in raw data of an alphabet page of an app. it then determines, whether or not
    the current page of the alphabet page has a "next" button. Returns None if it doesn't and
    returns the url of the next page if it does.
    """
    soup = BeautifulSoup(raw_data, HTML_PARSER)
    next_button = soup.find(class_=config_globals.next_button_class)
    if not next_button:
        return None
    return next_button.get('href', '')


def parse_information_list(information_list):
    information_dict = {}
    for information_div in information_list.findAll('div'):
        soup_key = information_div.find('dt')
        soup_value = information_div.find('dd')
        if soup_key and soup_value:
            key = soup_key.get_text().strip().lower()
            value = soup_value.get_text().strip()
            information_dict[key] = value
    return information_dict


def fill_app_data(app: App):
    """
    this function takes in an App object, and after using that object's url and parsing
    its page, fills in the rest of the information fields of the app.
    """
    raw_data = fetch_raw_data_requests(app.url)
    if raw_data is None:
        raise ValueError('got empty data from server')
    soup = BeautifulSoup(raw_data, HTML_PARSER)
    information_list = soup.find(class_=config_globals.key_information_list)
    if information_list:
        information_dict = parse_information_list(information_list)
        app.add_raw_info_dict(information_dict)

    raw_rating = soup.find(class_=config_globals.key_rating)
    raw_ratings_count = soup.find(class_=config_globals.key_ratings_count)
    if raw_rating and raw_ratings_count:
        rating = raw_rating.get_text()
        ratings_count = raw_ratings_count.get_text()
        if rating and ratings_count:
            app.add_rating_info(rating.strip(), ratings_count.strip())

    description = soup.find(class_=config_globals.key_description)
    if description and description.get_text():
        description_p = description.find('p')
        if description_p and description_p.get_text():
            app.add_description_info(description_p.get_text().strip())
        else:
            app.add_description_info(description.get_text().strip())


def enrich_data(app: App):
    """
    Enriching app with Google Custom Search API data for Play Store queries
    :param app:
    """
    request_params = {
        config_globals.GCS_QUERY_API_KEY: config_globals.gcs_api_keys[config_globals.gcs_api_current],
        config_globals.GCS_QUERY_SEARCH_ID: config_globals.gcs_engine_id,
        config_globals.GCS_QUERY_SEARCH_KEY: app.name
    }
    raw_data = fetch_raw_data_requests(config_globals.gcs_url, params=request_params)

    if raw_data is None:
        config_globals.gcs_api_current += 1
        if config_globals.gcs_api_current >= len(config_globals.gcs_api_keys):
            config_globals.gcs_api_current = 0
        raise ValueError('got empty data in enrich method')


    response_json = json.loads(raw_data.decode('utf-8'))
    for item in response_json.get('items', []):
        name = item['title']
        price = None
        rating = None
        ratings_count = None
        if app.name.lower() in name.lower() and 'pagemap' in item:
            params = item['pagemap']
            rating_list = params.get('aggregaterating', [])
            if type(rating_list) == list and len(rating_list) > 0:
                rating = rating_list[0].get('ratingvalue', None)
                ratings_count = rating_list[0].get('reviewcount', None)
            offer = params.get('offer', [])
            if type(offer) and len(offer) > 0:
                price = offer[0].get('price', None)
            app.set_play_store_info(PlayStoreInfo(name, price, rating, ratings_count))
            return True
    return False


def parse_apps_list(raw_data):
    """
    this function takes data of an app page, parse each of the 3 columns of apps, put the app in an App object.
    returns a list of the App objects.
    Queries price and in-app purchases into DB, no commit.
    """
    soup = BeautifulSoup(raw_data, HTML_PARSER)
    grid = soup.find(id=config_globals.apps_grid)
    columns = grid.findAll('div')
    parsed_list = []

    for column in columns:
        if column is None:
            continue
        for raw_app in column.findAll('li'):
            app = parse_primitive_entry(raw_app, App)
            try:
                fill_app_data(app)
            except ValueError as error:
                print(error)
                continue

            parsed_list.append(app)
            app_key = db_handler.insert_get_key(app.get_sql_dict(), 'apps', 'appstore_id')
            app.set_db_id(app_key)
            db_handler.save_regional_info(config_globals.region_db_id, app)
            if app.in_app_purchases and len(app.in_app_purchases) > 0:
                db_handler.save_in_app_purchases(config_globals.region_db_id, app)

            if config_globals.gcs_should_enrich:
                try:
                    if enrich_data(app):
                        db_handler.save_playstore_data(app)
                except ValueError as error:
                    print(error)

            if config_globals.verbose:
                print(app)

            if config_globals.db_commit_frequency > 0 and len(parsed_list) % config_globals.db_commit_frequency == 0:
                db_handler.commit()

            if config_globals.max_items_per_genre and len(parsed_list) == config_globals.max_items_per_genre:
                return parsed_list
    return parsed_list


def process_genre(genre):
    """
    given a genre object, the genre is processed. The function iterates over each
    page of the genre according to alphabet. In each page of the alphabet, the function iterates over
    the number of pages per alphabet page as long as the url exists. In every page, the apps are
    gathered into App objects and put in the list, which this function returns.
    Also writes genre in DB.
    """
    genre_key = db_handler.insert_get_key(genre.get_sql_dict(), 'genres')
    genre.set_db_id(genre_key)
    db_handler.commit()
    genre_apps_list = []
    raw_data = fetch_raw_data_requests(genre.url)
    if raw_data is None:
        raise ValueError('got empty data from server')
    if config_globals.mode == config_globals.MODE_ALL:
        alphabet = parse_list(raw_data, Letter, config_globals.target_letters)
        for letter in alphabet:
            print("Switching to letter " + letter.name)
            next_url = letter.url
            while next_url:
                raw_data = fetch_raw_data_requests(next_url)
                if raw_data is None:
                    raise ValueError('got empty data from server when parsing alphabetically ')
                genre_apps_list.extend(parse_apps_list(raw_data))
                db_handler.commit()
                if config_globals.max_items_per_genre and len(genre_apps_list) >= config_globals.max_items_per_genre:
                    return genre_apps_list
                next_url = find_next_button(raw_data)
    elif config_globals.mode == config_globals.MODE_POPULAR:
        genre_apps_list.extend(parse_apps_list(raw_data))
        db_handler.save_popular(config_globals.region_db_id, genre, genre_apps_list)
        db_handler.commit()

    return genre_apps_list


def iterate_genres(genres_list):
    """
    this function receives a list of genres and iterates over them and processes each genre
    with all its alphabet pages and each number page per alphabet page.
    generate a list of all the App objects with their descriptions.
    returns a dictionary of all the genres as keys and a list of the App objects as the value
    """
    apps_per_genre = {}
    limit = config_globals.max_genres if config_globals.max_genres > 0 else len(genres_list)
    for genre in genres_list[:limit]:
        try:
            genre_apps_list = process_genre(genre)
        except ValueError as genre_error:
            print(str(genre_error))
            continue
        apps_per_genre[genre] = genre_apps_list
    return apps_per_genre


def prepare_for_json(data):
    """Prepares final data for JSON export"""
    return {k.to_json(): [x.to_json() for x in v] for (k, v) in data.items()}


def export_to_json(json_data, output):
    """Writes json string to  output"""
    with open(output, 'w') as outfile:
        json.dump(json_data, outfile)


def parse_arguments():
    """Parsing arguments from command line"""
    parser = argparse.ArgumentParser()
    parser.add_argument(config_globals.ARG_MODE, choices=[config_globals.MODE_POPULAR, config_globals.MODE_ALL],
                        help='Mode of scraping', type=str)
    parser.add_argument(config_globals.ARG_REGION, help='AppStore Region, example: us', type=str)
    parser.add_argument('--{}'.format(config_globals.ARG_CONFIG), help='Alternative path to config file', type=str)
    parser.add_argument('--{}'.format(config_globals.ARG_DB), help='Alternative path to database file', type=str)
    parser.add_argument('--{}'.format(config_globals.ARG_JSON_FILE), help='Path to output json file', type=str)
    parser.add_argument('--{}'.format(config_globals.ARG_VERBOSE), help='Provide detailed output', action='store_true',
                        default=False)
    parser.add_argument('--{}'.format(config_globals.ARG_LETTERS), help='Scraping only specific letters', type=str,
                        nargs='+')
    parser.add_argument('--{}'.format(config_globals.ARG_GENRES), help='Scraping only specific genres', type=str,
                        nargs='+')
    arguments = vars(parser.parse_args())

    config_globals.mode = arguments[config_globals.ARG_MODE]
    config_globals.region_name = arguments[config_globals.ARG_REGION]
    config_globals.verbose = arguments[config_globals.ARG_VERBOSE]
    if arguments[config_globals.ARG_CONFIG]:
        config_globals.config_file_path = arguments[config_globals.ARG_CONFIG]
    if arguments[config_globals.ARG_DB]:
        config_globals.sqlite_db_file_path = arguments[config_globals.ARG_DB]
    if arguments[config_globals.ARG_LETTERS]:
        config_globals.target_letters = arguments[config_globals.ARG_LETTERS]
    if arguments[config_globals.ARG_GENRES]:
        config_globals.target_genres = arguments[config_globals.ARG_GENRES]
    if arguments[config_globals.ARG_JSON_FILE]:
        config_globals.json_file_name = arguments[config_globals.ARG_JSON_FILE]


def validate_arguments():
    """Checking that commandline arguments match with config ones"""
    if config_globals.region_name not in config_globals.regions.keys():
        raise ValueError('Wrong region provided, expected: [{}]'.format(', '.join(config_globals.regions.keys())))


def main():
    """
    main method. begins the scraping of the genres, followed by the scraping of each genre
    """
    global db_handler

    parse_arguments()
    if not config_handler.handle_config(config_globals.config_file_path):
        return
    validate_arguments()

    if config_globals.db_type == 'MySQL':
        db_handler = MySQLHandler()
    else:
        db_handler = SQLiteHandler()

    db_handler.configure_db()
    genres_list = parse_list(fetch_raw_data_requests(build_scrape_link()), Genre, config_globals.target_genres)
    data = iterate_genres(genres_list)
    db_handler.close_connection()
    if len(config_globals.json_file_name) > 0:
        json_data = prepare_for_json(data)
        export_to_json(json_data, config_globals.json_file_name)


if __name__ == '__main__':
    main()
