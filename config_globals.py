"""
File that contains all of the constants.
Authors: Alexey Shtern & Yonah Taurog
"""
# Command line block
MODE_ALL = 'all'
MODE_POPULAR = 'popular'
ARG_MODE = 'mode'
ARG_REGION = 'region'
ARG_VERBOSE = 'verbose'
ARG_LETTERS = 'letters'
ARG_GENRES = 'genres'
ARG_DB = 'db'
ARG_CONFIG = 'config'
ARG_JSON_FILE = 'jsonfile'

FREE_LABEL = 'free_label'
CURRENCY_MASK = 'currency_mask'
CURRENCY_NAME = 'currency_name'

config_file_path = 'base_config.ini'

db_type = ''
sqlite_db_file_path = 'apple_store.db'
db_commit_frequency = 0

mysql_user = ''
mysql_password = ''
mysql_host = ''
mysql_db_name = ''
mysql_port = None
mysql_socket = None

GCS_QUERY_API_KEY = 'key'
GCS_QUERY_SEARCH_ID = 'cx'
GCS_QUERY_SEARCH_KEY = 'q'

target_letters = []
target_genres = []

region_db_id = 0

# Config block
url = ''
region_name = ''
start_point = ''
max_retry = 0
mode = None
json_file_name = ''
verbose = False
max_genres = 0
max_items_per_genre = 0
should_parse_rating = False
should_parse_in_app_purchases = False
should_parse_description = False
info_fields = []
regions = {}
menu_class = ''
alphabet_list = ''
next_button_class = ''
apps_grid = ''
key_information_list = ''
key_description = ''
key_rating = ''
key_ratings_count = ''
key_in_app_purchases = ''

# Google Custom Search API
gcs_should_enrich = False
gcs_api_keys = ''
gcs_api_current = 0
gcs_engine_id = ''
gcs_url = ''
