"""
SQLite3 Database helper file.
Authors: Alexey Shtern & Yonah Taurog
"""

import sqlite3
import os
from pathlib import Path
import config_globals
from db_handler import DBHandler


class SQLiteHandler(DBHandler):

    def get_cursor(self):
        """Cursor singleton function"""
        if not self.__connection__:
            self.__connection__ = sqlite3.connect(config_globals.sqlite_db_file_path)
        if not self.__cursor__:
            self.__cursor__ = self.__connection__.cursor()
        return self.__cursor__

    def close_connection(self):
        """Close connection singleton function"""
        if not self.__connection__:
            self.__connection__.close()

    def commit(self):
        """Commit singleton function"""
        if self.__connection__:
            self.__connection__.commit()

    def configure_db(self):
        """Creates if needed and configures the DB"""
        my_file = Path(config_globals.sqlite_db_file_path)
        if not my_file.is_file():
            self.create_db()
        self.setup_region()
        # test_insert()
        # get_all_contents()

    def setup_region(self):
        """Loads regions from config to DB and performs necessary setup"""
        cursor = self.get_cursor()
        for name, params in config_globals.regions.items():
            cursor.execute(
                "INSERT OR IGNORE INTO regions(name, currency) VALUES "
                "(?,?)",
                [name, params[config_globals.CURRENCY_NAME]])
        self.commit()
        cursor.execute('SELECT id FROM regions WHERE name = (?)', [config_globals.region_name])
        config_globals.region_db_id = cursor.fetchone()[0]

    def delete_db(self):
        """Function for deleting DB file"""
        if os.path.exists(config_globals.sqlite_db_file_path):
            os.remove(config_globals.sqlite_db_file_path)

    def create_db(self):
        """Function that creates DB and all of the tables"""
        with sqlite3.connect(config_globals.sqlite_db_file_path) as con:
            cur = con.cursor()
            cur.execute('PRAGMA foreign_keys = ON;')

            cur.execute('''CREATE TABLE apps (
                                id INTEGER PRIMARY KEY,
                                name TEXT,
                                url TEXT,
                                appstore_id INTEGER,
                                seller TEXT,
                                size REAL,
                                age_rating INTEGER,
                                category TEXT,
                                compatibility TEXT,
                                copyright TEXT,
                                languages TEXT,
                                UNIQUE(appstore_id)
                                )''')

            cur.execute('''CREATE TABLE genres (
                                    id INTEGER PRIMARY KEY,
                                    name TEXT,
                                    url TEXT,
                                    UNIQUE(name)
                                    )''')

            cur.execute('''CREATE TABLE regions (
                                        id INTEGER PRIMARY KEY,
                                        name VARCHAR(200),
                                        currency VARCHAR(200),
                                        UNIQUE(name)
                                        )''')

            cur.execute('''CREATE TABLE popular (
                                    region_id INTEGER ,
                                    app_id INTEGER,
                                    genre_id INTEGER,
                                    position_nb INTEGER,
                                    FOREIGN KEY (region_id) REFERENCES regions(id),
                                    FOREIGN KEY (app_id) REFERENCES apps(id),
                                    FOREIGN KEY (genre_id) REFERENCES genres(id)
                                    )''')

            cur.execute('''CREATE TABLE regional_info (
                                        region_id INTEGER,
                                        app_id INTEGER,
                                        price REAL,
                                        rating REAL,
                                        ratings_count INTEGER,
                                        FOREIGN KEY (region_id) REFERENCES regions(id),
                                        FOREIGN KEY (app_id) REFERENCES apps(id),
                                        UNIQUE(region_id, app_id)
                                        )''')

            cur.execute('''CREATE TABLE in_app_purchases (
                                            id INTEGER PRIMARY KEY,
                                            region_id INTEGER,
                                            app_id INTEGER,
                                            name TEXT,
                                            price REAL,
                                            FOREIGN KEY (region_id) REFERENCES regions(id),
                                            FOREIGN KEY (app_id) REFERENCES apps(id)
                                            )''')

            cur.execute('''CREATE TABLE playstore_data (
                                                        id INTEGER AUTO_INCREMENT PRIMARY KEY,
                                                        name TEXT,
                                                        price REAL,
                                                        rating REAL,
                                                        ratings_count INTEGER,
                                                        app_id INTEGER,
                                                        FOREIGN KEY (app_id) REFERENCES apps(id),
                                                        UNIQUE(app_id)
                                                        )''')

    def insert_get_key(self, values_dict: dict, table, unique='name'):
        """Function for inserting or updating entries and getting DB ID"""
        cursor = self.get_cursor()
        keys = ' (' + ','.join(values_dict.keys())+') '
        values = list(values_dict.values())
        helper = '('+('?,'*len(values))[:-1]+')'

        try:
            query = 'INSERT INTO {} '.format(table) + keys + ' VALUES ' + helper
            cursor.execute(query, values)
            return cursor.lastrowid
        except sqlite3.IntegrityError:
            # handling the case if the entry is already in DB
            cursor.execute('SELECT id FROM {} WHERE {} = (?)'.format(table, unique), [values_dict[unique]])
            id_in_db = cursor.fetchone()[0]
            arguments = ','.join(["{} = \'{}\'".format(key, value) for key, value in values_dict.items()])
            query = 'UPDATE {} SET '.format(table) + arguments + ' WHERE id = (?)'
            cursor.execute(query, [id_in_db])

            return id_in_db

    def save_popular(self, region_id, genre, genre_apps_list):
        """Saving popular rankings into DB"""
        cursor = self.get_cursor()
        for i, app in enumerate(genre_apps_list):
            cursor.execute(
                "INSERT OR IGNORE INTO popular(region_id, app_id, genre_id, position_nb) VALUES "
                "(?,?,?,?)",
                [region_id, app.db_id, genre.db_id, i])
        self.commit()

    def save_in_app_purchases(self, region_id, app):
        """Saving In-App Purchases in DB. Doesn't have commit() intentionally"""
        cursor = self.get_cursor()
        for in_app_purchase in app.in_app_purchases:
            cursor.execute(
                "INSERT OR IGNORE INTO in_app_purchases (region_id, app_id, name, price) VALUES "
                "(?,?,?,?)",
                [region_id, app.db_id, in_app_purchase.name, in_app_purchase.price.get_price_parsed()])

    def save_regional_info(self, region_id, app):
        """Saving regional info in DB. Doesn't have commit() intentionally"""
        cursor = self.get_cursor()
        cursor.execute(
            "INSERT OR IGNORE INTO regional_info (region_id, app_id, price, rating, ratings_count) VALUES "
            "(?,?,?,?,?)",
            [region_id, app.db_id, app.get_formatted_price(), app.rating, app.ratings_count])

    def save_playstore_data(self, app):
        """Saving PlayStore data for specified app"""
        if not app.play_store_info:
            return
        data = app.play_store_info
        cursor = self.get_cursor()
        cursor.execute(
            "INSERT OR IGNORE INTO playstore_data (name, price, rating, ratings_count, app_id) VALUES "
            "(?,?,?,?,?)",
            [data.ps_name, data.ps_price, data.ps_rating, data.ps_ratings_count, app.db_id])

    def test_insert(self):
        """Dummy insert function"""
        with sqlite3.connect(config_globals.sqlite_db_file_path) as con:
            cur = con.cursor()
            cur.execute('PRAGMA foreign_keys = ON;')
            app_dict = {'appstore_id': '1234567',
                        'name': 'ITC', 'seller': 'Alexey Shtern', 'size': 2, 'age_rating': 18,
                        'category': 'Games', 'compatibility': 'iOS 1+', 'copyright': 'all rights reserved',
                        'languages': 'English/Russian'}

            app_key = self.insert_get_key(cur, app_dict, 'apps', 'appstore_id')
            genre_key = self.insert_get_key(cur, {'name': 'Games'}, 'genres')
            region_key = self.insert_get_key(cur, {'name': 'United States', 'currency': 'USD'}, 'regions')

            cur.execute(
                "INSERT OR IGNORE INTO popular(region_id, app_id, genre_id, position_nb) VALUES "
                "(?,?,?,?)",
                [region_key, app_key, genre_key, 4])

            cur.execute(
                "INSERT OR IGNORE INTO regional_info (region_id, app_id, price, rating, ratings_count) VALUES "
                "(?,?,?)",
                [region_key, app_key, 5.0, 4.7, 1000])

            cur.execute(
                "INSERT OR IGNORE INTO in_app_purchases (region_id, app_id, name, price) VALUES "
                "(?,?,?,?)",
                [0, app_key, 'ITC coins', 5000.0])

            con.commit()

    def get_all_contents(self):
        """Print all tables' contents"""
        with sqlite3.connect(config_globals.sqlite_db_file_path) as con:
            cur = con.cursor()
            cur.execute('SELECT * FROM apps')
            print(cur.fetchall())
            cur.execute('SELECT * FROM genres')
            print(cur.fetchall())
            cur.execute('SELECT * FROM regions')
            print(cur.fetchall())
            cur.execute('SELECT * FROM popular')
            print(cur.fetchall())
            cur.execute('SELECT * FROM regional_info')
            print(cur.fetchall())
            cur.execute('SELECT * FROM in_app_purchases')
            print(cur.fetchall())





